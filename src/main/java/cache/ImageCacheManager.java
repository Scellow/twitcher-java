package cache;

import javafx.scene.image.Image;

import java.util.HashMap;

public class ImageCacheManager
{
    private static HashMap<String, Image> imageHashMap = new HashMap<String, Image>();

    public static Image cacheImage(String game, Image image)
    {
        if (imageHashMap.containsKey(game))
            return imageHashMap.get(game);
        else
        {
            imageHashMap.put(game, image);
            return image;
        }
    }

    public static Image getImage(String game)
    {
        if (imageHashMap.containsKey(game))
        {
            return imageHashMap.get(game);
        }
        return null;
    }
}
