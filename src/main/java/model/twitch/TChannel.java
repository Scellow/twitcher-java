package model.twitch;

public class TChannel
{
    public Boolean mature;
    public Object delay;
    public String status;
    public String broadcaster_language;
    public String display_name;
    public String game;
    public String language;
    public int _id;
    public String name;
    public String created_at;
    public String updated_at;
    public String logo;
    public String banner;
    public String video_banner;
    public Object background;
    public String profile_banner;
    public String profile_banner_background_color;
    public Boolean partner;
    public String url;
    public int views;
    public int followers;
    public TLinksChannel _links;
}