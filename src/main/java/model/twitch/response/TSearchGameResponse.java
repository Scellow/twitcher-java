package model.twitch.response;

import model.twitch.TGame;
import model.twitch.TLinksSearchResponse;

import java.util.ArrayList;
import java.util.List;

public class TSearchGameResponse
{
    public TLinksSearchResponse _links;
    public List<TGame> games = new ArrayList<TGame>();
}
