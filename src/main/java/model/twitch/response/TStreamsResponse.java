package model.twitch.response;

import model.twitch.TLinksStreamResponse;
import model.twitch.TStream;

import java.util.ArrayList;
import java.util.List;

public class TStreamsResponse
{
    public int _total;
    public List<TStream> streams = new ArrayList<TStream>();
    public TLinksStreamResponse _links;
}
