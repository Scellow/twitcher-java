package model.twitch;

public class TStream
{
    public String game;
    public int viewers;
    public double average_fps;
    public int video_height;
    public String created_at;
    public long _id;
    public TChannel channel;
    public TPreview preview;
    public TLinksStream _links;
}
