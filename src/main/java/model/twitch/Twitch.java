package model.twitch;

public class Twitch
{
    public static final String ACCEPT_HEADER = "application/vnd.twitchtv.v3+json";

    public static final String BASE_URL = "https://api.twitch.tv/kraken";

    public static final String SEARCH_CHANNELS = "/search/channels";
    public static final String SEARCH_STREAMS = "/search/streams";
    public static final String SEARCH_GAMES = "/search/games";
}
