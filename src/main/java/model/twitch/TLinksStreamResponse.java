package model.twitch;

public class TLinksStreamResponse
{
    public String summary;
    public String followed;
    public String next;
    public String featured;
    public String self;
}