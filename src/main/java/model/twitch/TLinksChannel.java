package model.twitch;

public class TLinksChannel
{
    public String self;
    public String follows;
    public String commercial;
    public String stream_key;
    public String chat;
    public String features;
    public String subscriptions;
    public String editors;
    public String teams;
    public String videos;
}