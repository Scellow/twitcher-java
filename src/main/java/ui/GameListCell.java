package ui;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import model.twitch.TGame;
import util.AsyncImageProperty;

import java.util.HashMap;


public class GameListCell extends ListCell<TGame>
{
    private ImageView gameIcon;
    private Image tmp;

    private TGame tGame;
    private HashMap<String, Image> test = new HashMap<String, Image>();

    private AsyncImageProperty imageProperty = new AsyncImageProperty();
    private FadeTransition gameCellFadeFin;
    private ScaleTransition transition;

    private boolean imageLoaded = false;
    private int cellHeight = 72;

    public GameListCell()
    {
        setPrefHeight(cellHeight);
        gameCellFadeFin = new FadeTransition(Duration.millis(2000));
        gameCellFadeFin.setNode(this);
        gameCellFadeFin.setFromValue(0);
        gameCellFadeFin.setToValue(1.0);
        gameCellFadeFin.setCycleCount(1);
        gameCellFadeFin.setAutoReverse(false);
        //gameCellFadeFin.playFromStart();

        gameIcon = new ImageView();

        imageProperty = new AsyncImageProperty();
        imageProperty.setListener(new AsyncImageProperty.ImageLoadedListener()
        {
            public void imageLoaded()
            {
                imageLoaded = true;
            }
        });

    }

    @Override
    public void updateIndex(int i)
    {
        super.updateIndex(i);
        if (tGame == null) return;
        if (!test.containsKey(tGame.name))
            loadImage(tGame);
    }

    public void updateItem(final TGame tGame, boolean empty)
    {
        super.updateItem(tGame, empty);

        if (empty)
        {
            setText(null);
            setGraphic(null);
            setPrefHeight(0);
        } else
        {
            if (gameIcon.getImage() == null) loadImage(tGame);
            setText(tGame.name);
            setGraphic(gameIcon);
            this.tGame = tGame;
            setPrefHeight(cellHeight);
        }
    }

    @Override
    protected boolean isItemChanged(TGame oldItem, TGame newItem)
    {
        //gameIcon.setImage(new Image(newItem.box.small));
        return super.isItemChanged(oldItem, newItem);
    }

    public void loadImage(final TGame tGame)
    {
        gameIcon.imageProperty().bind(imageProperty);
        imageProperty.imageUrlProperty().set(tGame.box.small);
        test.put(tGame.name, gameIcon.getImage());
    }

}


