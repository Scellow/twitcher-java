package ui;

import controller.StreamController;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import model.twitch.TStream;
import util.AsyncImageProperty;

import java.io.IOException;
import java.util.HashMap;


public class StreamListCell extends ListCell<TStream>
{


    private AsyncImageProperty imageProperty;
    private HashMap<Long, Image> test = new HashMap<Long, Image>();
    private TStream tStream;

    private int cellHeight = 440;

    private StreamController streamController;

    private boolean imageLoaded = false;

    public StreamListCell()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(
                getClass().getResource("/streamView.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        streamController = fxmlLoader.getController();

        imageProperty = new AsyncImageProperty();
        imageProperty.setListener(new AsyncImageProperty.ImageLoadedListener()
        {
            public void imageLoaded()
            {
                imageLoaded = true;
            }
        });
    }

    @Override
    public void updateIndex(int i)
    {
        super.updateIndex(i);
        if (tStream == null) return;
        if (!test.containsKey(tStream._id))
        {
            loadImage(tStream);
            loadTitle(tStream);
        }
    }



    @Override
    public void updateItem(TStream tStream, boolean empty)
    {
        super.updateItem(tStream, empty);

        if (empty)
        {
            setText(null);
            setGraphic(null);
            setPrefHeight(0);
        } else
        {
            setPrefHeight(cellHeight);
            if (streamController.getStreamPreview() != null)
                if (streamController.getStreamPreview().getImage() == null)
                {
                    loadImage(tStream);
                    loadTitle(tStream);
                }
            this.tStream = tStream;
            setText(null);
            setGraphic(streamController.getCell());
        }
    }


    public void loadImage(final TStream tStream)
    {
        streamController.getStreamPreview().imageProperty().bind(imageProperty);
        imageProperty.imageUrlProperty().set(tStream.preview.large);
    }

    private void loadTitle(TStream tStream)
    {
        streamController.getStreamTitle().setText(tStream.channel.name);
    }
}
