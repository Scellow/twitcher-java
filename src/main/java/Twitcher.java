import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Twitcher extends Application
{

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        Parent root = FXMLLoader.load(getClass().getResource("mainView.fxml"));

        Scene scene = new Scene(root, 980, 640);
        primaryStage.setTitle("Twitcher - v0.1");
        primaryStage.setScene(scene);
        primaryStage.setMinWidth(730);
        primaryStage.setMinHeight(320);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>()
        {
            public void handle(WindowEvent event)
            {
                Platform.exit();
                System.exit(0);
            }
        });
        primaryStage.show();
    }
}