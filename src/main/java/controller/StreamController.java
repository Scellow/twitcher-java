package controller;

import javafx.animation.TranslateTransition;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class StreamController implements Initializable
{
    @FXML
    private VBox cell;
    @FXML
    private ImageView ivStreamPreview;
    @FXML
    private Label lblStreamTitle;
    @FXML
    private Button btnExpandStreamView;

    private TranslateTransition transition;
    private boolean expand = false;

    public void initialize(URL location, ResourceBundle resources)
    {
        transition = new TranslateTransition(Duration.millis(500));
        transition.setNode(ivStreamPreview);

    }

    public VBox getCell()
    {
        return this.cell;
    }

    public ImageView getStreamPreview()
    {
        return this.ivStreamPreview;
    }

    public Label getStreamTitle()
    {
        return this.lblStreamTitle;
    }

    public void btnExpandStreamView_On_Action(ActionEvent event)
    {
        expand = !expand;

        if(expand)
        {
            btnExpandStreamView.setText("<");
            transition.setFromX(ivStreamPreview.getX());
            transition.setToX(85);
            transition.playFromStart();
        }
        else
        {
            btnExpandStreamView.setText(">");
            transition.setFromX(ivStreamPreview.getX());
            transition.setToX(0);
            transition.playFromStart();
        }
    }
}
