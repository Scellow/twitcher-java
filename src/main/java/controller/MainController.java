package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import model.twitch.TGame;
import model.twitch.TStream;
import model.twitch.Twitch;
import model.twitch.response.TSearchGameResponse;
import model.twitch.response.TStreamsResponse;
import ui.GameListCell;
import ui.StreamListCell;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Future;

public class MainController implements Initializable
{
    private List<Future<HttpResponse<String>>> queuRequest = new ArrayList<Future<HttpResponse<String>>>();
    private Thread updateUIThread;

    private TGame selectedGame;
    private TStream selectedStream;

    private ObservableList<TGame> gameObservableList = FXCollections.observableArrayList();
    private ObservableList<TStream> streamObservableList = FXCollections.observableArrayList();

    @FXML
    private TextField tfSearchGame;


    @FXML
    private ListView<TGame> lvGames;

    @FXML
    private ListView<TStream> lvStreams;


    public void initialize(URL location, ResourceBundle resources)
    {
        updateUIThread = new Thread();

        lvGames.setCellFactory(new Callback<ListView<TGame>, ListCell<TGame>>()
        {
            public ListCell<TGame> call(ListView<TGame> param)
            {
                return new GameListCell();
            }
        });
        lvGames.setItems(gameObservableList);

        lvStreams.setCellFactory(new Callback<ListView<TStream>, ListCell<TStream>>()
        {
            public ListCell<TStream> call(ListView<TStream> param)
            {
                return new StreamListCell();
            }
        });
        lvStreams.setItems(streamObservableList);

        tfSearchGame.setFocusTraversable(false);

    }

    public void tfSearchGame_On_Key_Typed(KeyEvent event)
    {

        String query = tfSearchGame.getText();
        if (event.getCode() != KeyCode.DELETE)
            query += event.getCharacter();

        if (query.length() == 0) return;

        searchGame(query);


    }

    public void lvGames_On_Mouse_Clicked(Event event)
    {
        if (lvGames.getItems().size() == 0) return;

        TGame selectedGame = lvGames.getSelectionModel().getSelectedItem();

        if (selectedGame != null)
        {
            System.out.println("INFO: Game Selected: " + selectedGame.name);
            if (this.selectedGame != null)
                if (this.selectedGame == selectedGame)
                    return;
            this.selectedGame = selectedGame;

            String game = selectedGame.name;

            //populate lvStreams
            getStreams(game);
        }

    }


    //region SEND REQUESTS

    public void searchGame(String query)
    {
        //gameObservableList.clear();

        for (Future<HttpResponse<String>> req : queuRequest)
        {
            req.cancel(true);
        }

        queuRequest.add(Unirest.get(Twitch.BASE_URL + Twitch.SEARCH_GAMES)
                .header("accept", Twitch.ACCEPT_HEADER)
                .queryString("query", query)
                .queryString("type", "suggest")
                .asStringAsync(new com.mashape.unirest.http.async.Callback<String>()
                {
                    public void completed(final HttpResponse<String> httpResponse)
                    {
                        Platform.runLater(new Runnable()
                        {
                            public void run()
                            {
                                try
                                {
                                    if (httpResponse.getStatus() == 503)
                                    {
                                        System.out.println("ERROR: 503 Unable to search games");
                                        return;
                                    }
                                    TSearchGameResponse tsgr = new ObjectMapper().readValue(httpResponse.getBody(), TSearchGameResponse.class);

                                    gameObservableList.setAll(tsgr.games);

                                    //lvGames.edit(0);
                                } catch (IOException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }

                    public void failed(UnirestException e)
                    {

                    }

                    public void cancelled()
                    {

                    }
                }));

    }

    public void getStreams(String game)
    {
        streamObservableList.clear();

        for (Future<HttpResponse<String>> req : queuRequest)
        {
            req.cancel(true);
        }

        queuRequest.add(Unirest.get(Twitch.BASE_URL + Twitch.SEARCH_STREAMS)
                .header("accept", Twitch.ACCEPT_HEADER)
                .queryString("query", game)
                .asStringAsync(new com.mashape.unirest.http.async.Callback<String>()
                {
                    public void completed(final HttpResponse<String> httpResponse)
                    {
                        Platform.runLater(new Runnable()
                        {
                            public void run()
                            {
                                TStreamsResponse tsgr = null;
                                try
                                {
                                    tsgr = new ObjectMapper().readValue(httpResponse.getBody(), TStreamsResponse.class);
                                    streamObservableList.addAll(tsgr.streams);
                                } catch (IOException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }

                    public void failed(UnirestException e)
                    {

                    }

                    public void cancelled()
                    {

                    }
                }));

    }

    public void tfSearchGame_On_Mouse_Entered(Event event)
    {

    }

    public void tfSearchGame_On_Mouse_Exited(Event event)
    {

    }

    //endregion
}

