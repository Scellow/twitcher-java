package listerner;

import model.twitch.TGame;

public interface IGameSelection
{
    void onSelection(TGame game);
}
