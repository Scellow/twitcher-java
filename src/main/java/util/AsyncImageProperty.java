package util;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.scene.image.Image;

public class AsyncImageProperty extends SimpleObjectProperty<Image>
{
    private final ImageLoadService imageLoadService = new ImageLoadService();
    private final ObjectProperty<String> imageUrl = new SimpleObjectProperty<String>();
    private ImageLoadedListener listener;

    public AsyncImageProperty()
    {
        imageLoadService.stateProperty().addListener(new ChangeListener<Worker.State>()
        {

            public void changed(ObservableValue<? extends Worker.State> observable, Worker.State oldValue, Worker.State value)
            {
                if (value == Worker.State.SUCCEEDED)
                {
                    listener.imageLoaded();
                    set(imageLoadService.getValue());
                }
                if (value == Worker.State.FAILED)
                {
                    set(null);
                }
                if (value == Worker.State.SUCCEEDED || value == Worker.State.CANCELLED || value == Worker.State.FAILED)
                {
                    String handle = imageUrl.get();
                    if (handle != null && !handle.equals(imageLoadService.imageUrl))
                    {
                        loadImageInBackground(handle);
                    }
                }
            }
        });

        imageUrl.addListener(new ChangeListener<String>()
        {
            public void changed(ObservableValue<? extends String> observable, String oldValue, String value)
            {
                if (!imageLoadService.isRunning())
                {
                    loadImageInBackground(imageUrl.getValue());
                }
            }
        });
    }

    public void setListener(ImageLoadedListener listener)
    {
        this.listener = listener;
    }

    public ObjectProperty<String> imageUrlProperty()
    {
        return imageUrl;
    }

    private void loadImageInBackground(String imageUrl)
    {
        synchronized (imageLoadService)
        {
            if (imageUrl != null)
            {
                imageLoadService.setImageUrl(imageUrl);
                imageLoadService.restart();
            }
        }
    }

    public interface ImageLoadedListener
    {
        void imageLoaded();
    }

    private static class ImageLoadService extends Service<Image>
    {
        private String imageUrl;

        public void setImageUrl(String imageUrl)
        {
            this.imageUrl = imageUrl;
        }

        @Override
        protected Task<Image> createTask()
        {
            final String imageUrl = this.imageUrl;

            return new Task<Image>()
            {
                @Override
                protected Image call()
                {
                    return new Image(imageUrl);
                }
            };
        }
    }
}